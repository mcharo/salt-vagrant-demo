=================
Salt Vagrant Demo
=================

A Salt Demo using Vagrant.


Instructions
============

Run the following commands in a terminal. Git, VirtualBox and Vagrant must
already be installed.

.. code-block:: bash

    git clone https://gitlab.com/mcharo/salt-vagrant-demo.git
    cd salt-vagrant-demo
    vagrant up


This will download Windows and Ubuntu VirtualBox images and create four virtual
machines for you. One will be a Salt Master named `master` and three will be Salt
Minions named `minion1`, `minion2`, and `minion3`.  `minion1` and `minion2` are Ubuntu minions, and `minion3` is a Windows minion. The Salt Minions will point to the Salt
Master and the Minion's keys will already be accepted. Because the keys are
pre-generated and reside in the repo, please be sure to regenerate new keys if
you use this for production purposes.

You can then run the following commands to log into the Salt Master and begin
using Salt.

.. code-block:: bash

    vagrant ssh master
    sudo salt \* test.ping
